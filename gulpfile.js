var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var notify	= require('gulp-notify');
var autoprefixer = require('autoprefixer');
var postcss = require('gulp-postcss');
var runSequence = require('run-sequence');
var cssbeautify = require('gulp-cssbeautify');
var concat = require('gulp-concat');
 
gulp.task('sass', function () {
  return gulp.src('src/scss/style.scss') //style.scss when all styles imported
	.pipe(sourcemaps.init())
    .pipe(sass().on('error', notify.onError(function (error) {
          return 'Error compiling LESS: ' + error.message;
    })))
    .pipe(postcss([ autoprefixer({
        browsers: ['> 1%', 'last 3 versions', 'Firefox >= 20', 'iOS >=7']
    }) ]))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest('./css'))
    .pipe(notify({ message: 'Successfully compiled SASS' }));
});

gulp.task('prettifycss', function() {
    return gulp.src('./css/*.css')
        .pipe(cssbeautify())
        .pipe(gulp.dest('./css'));
});

gulp.task('js', function() {
	return gulp.src('src/js/*.js')
    .pipe(concat('scripts.js'))
	.pipe(gulp.dest('js'))
	.pipe(notify({ message: 'Successfully compiled JavaScript' }));
});

gulp.task('build-sass', function() {
  runSequence('sass','prettifycss');
});

gulp.task('sass:watch', function () {
	gulp.watch('style.scss', ['build-sass']);
});